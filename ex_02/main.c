#include <stdio.h>
#include <stdlib.h>

int	main(int ac, char **av)
{
  int	i;
  int	nb;

  if (ac < 2)
    {
      printf("Error.\n");
      return (-1);
    }
  nb = atoi(av[1]);
  if (nb <= 0)
    {
      printf("Error.\n");
      return (-1);
    }
  i = -1;
  while (++i < nb)
    printf("Hello\n");
  return (0);
}
